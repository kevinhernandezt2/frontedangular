import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-finanza-tarjeta',
  templateUrl: './finanza-tarjeta.component.html'
})
export class FinanzaTarjetaComponent implements OnInit {

  @Input() content: any = {};
  @Input() index: number;

  @Output() seleccion: EventEmitter<number>;

  constructor(private router: Router) {
    this.seleccion = new EventEmitter();
  }

  ngOnInit() {
  }

  verFinanza() {
    
  this.router.navigate( ['/contenido1', this.index] );
 
 
}

}
