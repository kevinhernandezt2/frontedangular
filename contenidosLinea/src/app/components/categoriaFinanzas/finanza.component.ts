import { Component, OnInit } from '@angular/core';
import { contentServices, ContenidoFinanzas } from '../../servicios/content.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-finanza',
  templateUrl: './finanza.component.html'
})
export class FinancieComponent implements OnInit {
  
  finanza:ContenidoFinanzas[] = [];

  constructor( private _contentService:contentServices,
               private router:Router
                ) {
  }

  ngOnInit() {
    this.finanza = this._contentService.getFinanzas();
  }

  verFinanza( idx:number ){
    this.router.navigate( ['/contenido1',idx] );
  }

}