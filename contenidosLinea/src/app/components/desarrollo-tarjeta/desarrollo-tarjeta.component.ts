import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-desarrollo-tarjeta',
  templateUrl: './desarrollo-tarjeta.component.html'
})
export class DesarrolloTarjetaComponent implements OnInit {

  @Input() content: any = {};
  @Input() index: number;

  @Output() seleccion: EventEmitter<number>;

  constructor(private router: Router) {
    this.seleccion = new EventEmitter();
  }

  ngOnInit() {
  }

  verDesarrollo() {
    
      this.router.navigate( ['/contenido', this.index] );
   
   
  }

}
