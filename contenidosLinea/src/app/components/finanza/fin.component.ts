import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

import { contentServices } from '../../servicios/content.service';

@Component({
  selector: 'app-fin',
  templateUrl: './fin.component.html'
})
export class FinComponent {

  finanza:any = {};
 

  constructor( private activatedRoute: ActivatedRoute,
    private _contentService: contentServices
){

this.activatedRoute.params.subscribe( params =>{
this.finanza = this._contentService.getFinanzasId( params['id'] );
});

}

}
