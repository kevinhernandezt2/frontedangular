import { Component, OnInit } from '@angular/core';
import { contentServices, Contenido } from '../../servicios/content.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-desarrollo',
  templateUrl: './desarrollo.component.html'
})
export class DevelopingComponent implements OnInit {
  
  desarrollo:Contenido[] = [];

  constructor( private _contentService:contentServices,
               private router:Router
                ) {
  }

  ngOnInit() {
    this.desarrollo = this._contentService.getDesarrollo();
  }

  verDesarrollo( idx:number ){
    this.router.navigate( ['/contenido',idx] );
  }

}