import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

import { contentServices } from '../../servicios/content.service';

@Component({
  selector: 'app-des',
  templateUrl: './des.component.html'
})
export class DesComponent {

  desarrollo:any = {};
 

  constructor( private activatedRoute: ActivatedRoute,
    private _contentService: contentServices
){

this.activatedRoute.params.subscribe( params =>{
this.desarrollo = this._contentService.getDesarrolloId( params['id'] );
});

}

}
