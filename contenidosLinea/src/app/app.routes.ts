
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DevelopingComponent } from './components/categoriaDesarrollos/desarrollo.component';
import { DesComponent } from './components/desarrollo/des.component';
import { FinComponent } from './components/finanza/fin.component';
import { FinancieComponent } from './components/categoriaFinanzas/finanza.component';
import { from } from 'rxjs';
const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'desarrollo', component: DevelopingComponent },
  { path: 'finanza', component: FinancieComponent },
  { path: 'contenido/:id', component: DesComponent },
  { path: 'contenido1/:id', component: FinComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }

];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
