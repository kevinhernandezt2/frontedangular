import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Rutas
import { APP_ROUTING } from './app.routes';

// Servicios
import { contentServices } from './servicios/content.service';

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { DevelopingComponent } from './components/categoriaDesarrollos/desarrollo.component';
import { DesComponent } from './components/desarrollo/des.component';
import { DesarrolloTarjetaComponent } from './components/desarrollo-tarjeta/desarrollo-tarjeta.component';
import { FinanzaTarjetaComponent } from './components/finanza-tarjeta/finanza-tarjeta.component';
import { FinancieComponent } from './components/categoriaFinanzas/finanza.component';
import { FinComponent } from './components/finanza/fin.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    DevelopingComponent,
    DesComponent,
    DesarrolloTarjetaComponent,
    FinanzaTarjetaComponent,
    FinancieComponent,
    FinComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [
    contentServices
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
