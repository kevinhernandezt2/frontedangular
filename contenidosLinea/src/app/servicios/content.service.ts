import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class contentServices {



  private desarrollo:Contenido[] = [
    {
      img: "assets/img/angular.png",
      title: "ANGULAR",
      description: "Angular es un marco de trabajo (framework) de front-end impulsado por Google. Creado para desarrollar aplicaciones web, aplicaciones móviles",
      dateTime: "2020-07-15"
    },
    {
      img: "assets/img/Python.png",
      title: "PYTHON",
      description: "Python es un lenguaje de programación interpretado, por lo que funciona en cualquier tipo de sistema que integre su interpretador.",
      dateTime: "2020-07-09"
    },
    {
      img: "assets/img/java.png",
      title: "JAVA",
      description: "Java es un lenguaje de programación orientado a objetos. La idea de Java es que pueda realizarse programas con la posibilidad de ejecutarse",
      dateTime: "2020-07-20"
    }
  ];

  private finanzas:ContenidoFinanzas[] = [
    {
      img: "assets/img/contabilidad.jpg",
      title: "CONTABILIDAD",
      description: "Sistema de control y registro de los gastos e ingresos y demás operaciones económicas que realiza una empresa o entidad.",
      dateTime: "2020-07-20"
    },
    {
      img: "assets/img/excel.jpg",
      title: "EXCEL",
      description: "Excel es un programa del tipo Hoja de Cálculo que permite realizar operaciones con números organizados en una cuadrícula.",
      dateTime: "2020-07-09"
    },
    {
      img: "assets/img/impuesto.jpg",
      title: "IMPUESTOS",
      description: "Los impuestos, en la mayoría de legislaciones surgen exclusivamente por la “potestad tributaria del Estado",
      dateTime: "2020-07-20"
    }
  ];


  constructor() {
    console.log("Servicio listo para usar!!!");
  }


  getDesarrollo():Contenido[]{
    return this.desarrollo;
  }

  getFinanzas():ContenidoFinanzas[]{
    return this.finanzas;
  }

  getDesarrolloId( idx: string ){
    return this.desarrollo[idx];
  }

  getFinanzasId( idx: string ){
    return this.finanzas[idx];
  }

  buscarContenidos( termino:string ):Contenido[]{

    let contenidoArr:Contenido[] = [];
    termino = termino.toUpperCase();

    for( let i = 0; i < this.desarrollo.length; i ++ ){

      let developing = this.desarrollo[i];

      let nombre = developing.title.toUpperCase();

      if( nombre.indexOf( termino ) >= 0  ){
        developing.idx = i;
        contenidoArr.push( developing )
      }

    }

    for( let i = 0; i < this.finanzas.length; i ++ ){

      let financie = this.finanzas[i];

      let nombre = financie.title.toUpperCase();

      if( nombre.indexOf( termino ) >= 0  ){
        financie.idx = i;
        contenidoArr.push( financie )
      }

    }

    return contenidoArr;

  }


}


export interface Contenido{
  img: string;
  title: string;
  description: string;
  dateTime: string;
  idx?: number;
};

export interface ContenidoFinanzas{
  img: string;
  title: string;
  description: string;
  dateTime: string;
  idx?: number;
};

