from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

import psycopg2
import psycopg2.extras

app = Flask(__name__)

"""
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:(lyon_team123)@127.0.0.1/bdcontenidos'
db = SQLAlchemy(app)

CORS(app)


#metodo que se ejecuta como corresponde SQLAlchemy -> AttributeError: 'SQLAlchemy' object has no attribute 'connect'
@app.route('/api/categorias', methods=['GET'])
def get_all_categories():

    cur = db.connection.cursor()

    cur.execute("SELECT * FROM contenidos.tbcontenido")

    results = cur.fetchall()

    return jsonify(results)
"""
CORS(app)

def connectDB():

    connectionString = 'dbname=bdcontenidos user=postgres password=(lyon_team123) host=127.0.0.1 port=5432'
    print(connectionString)

    try:
        return psycopg2.connect(connectionString)
    except:
        print("No puede conectarse a la base de datos")


@app.route('/api/usuario', methods=['POST'])
def get_user():

    conexion = connectDB()
    cur = conexion.cursor()

    usuario = request.get_json()['user']
    contrasena = request.get_json()['password']

    if ((usuario == "" or contrasena == "") or (usuario == None or contrasena == None)):

        variable = "usuario o contrasena vacios o nulos"

        return jsonify(variable)

    else:

        cur.execute("SELECT usuario, contrasena FROM contenidos.tbusuario"
                    +" WHERE usuario = '"+ str(usuario) +"' AND contrasena = '"+ str(contrasena) +"'")
        try:
            results = cur.fetchone()
            print(results)

        except:
            print("hubo error en la consulta o usuario no existe")

        return jsonify(results)
        

@app.route('/api/categorias', methods=['POST'])
def get_all_categories():

    conexion = connectDB()
    cur = conexion.cursor()


    cur.execute("SELECT * FROM contenidos.tbcategoria")

    try:
        results = cur.fetchall()
        print(results)
    except:
        print("hubo error en la consulta o categoria no existe")
        
    return jsonify(results)

@app.route('/api/content-idcategory/<int:id_category>', methods=['POST'])
def get_filter_content_category_id(id_category):

    conexion = connectDB()
    cur = conexion.cursor()
    
    cur.execute("SELECT contenido_pk, imagen, titulo, descripcion, enlace, etiquetas, fecha_limite FROM contenidos.tbcontenido"
                +" WHERE categoria_fk = " + str(id_category))
    try:
        results = cur.fetchall()
        print(results)

    except:
        results = "hubo error en la consulta o contenidos por categoria id no existe"


    return jsonify(results)

@app.route('/api/content-idcategory-optiontwo', methods=['POST'])
def get_filter_contentCategoryIdOption():

    conexion = connectDB()
    cur = conexion.cursor()
    
    categoria_fk = request.get_json()['id_category']

    if (categoria_fk == 0 or categoria_fk == None or categoria_fk == ""):

        variable = "categoria seleccionada no existe"

        return jsonify(variable)

    else:
    
        cur.execute("SELECT contenido_pk, imagen, titulo, descripcion, enlace, etiquetas, fecha_limite FROM contenidos.tbcontenido"
                    +" WHERE categoria_fk = '"+ str(categoria_fk) +"'")
        try:
            results = cur.fetchall()
            print(results)

        except:
            results = "hubo error en la consulta o contenidos por categoria id no existe"

        return jsonify(results)

@app.route('/api/content-id/<int:id_content>', methods=['POST'])
def get_filter_content_id(id_content):

    conexion = connectDB()
    cur = conexion.cursor()

    cur.execute("SELECT contenido_pk, imagen, titulo, descripcion, enlace, etiquetas, fecha_limite FROM contenidos.tbcontenido"
                +" WHERE contenido_pk = " + str(id_content))
    try:
        results = cur.fetchall()
        print(results)

    except:
        results = "hubo error en la consulta o contenido por id no existe"

    return jsonify(results)

@app.route('/api/content-detail', methods=['POST'])
def get_all_contens_detail():

    conexion = connectDB()
    cur = conexion.cursor()

    cur.execute("SELECT titulo, hojadetalle FROM contenidos.tbcontenido")

    try:
        results = cur.fetchall()
        print(results)

    except:
        results = "hubo error en la consulta o detalles de los contenidos no existe"

    return jsonify(results)



if __name__ == '__main__':
    app.run(debug=True)
